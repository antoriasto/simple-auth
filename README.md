## About

This project was created just for learning web development stuff in php.
These are several framework ive been using for this project just to make my life easier:

- [Laravel](https://laravel.com).
- [Spatie Laravel Permission](https://github.com/spatie/laravel-permission).
- [Twitter Bootsrap](https://github.com/twbs/bootstrap).

## Live Demo
- [http://enigmatic-headland-06384.herokuapp.com](http://enigmatic-headland-06384.herokuapp.com/articles)

## Installation

* clone project
* ``` composer install && npm install ```
* create database
* create .env file configuration & setting db connection
* ``` php artisan migrate ```
* ``` php artisan db:seed ```
* ``` php artisan serve ```

## Screenshoot

![Users](https://bitbucket.org/antoriasto/simple-auth/raw/8e341dba06cd5f69061e9564d498347b2654bcb9/docs/images/users.png)

![Edit roles user](https://bitbucket.org/antoriasto/simple-auth/raw/ea45d31c924d81da3ff10c7d3961758005e3cec9/docs/images/edit-user-roles.png)

