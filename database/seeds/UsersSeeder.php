<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;

class UsersSeeder extends Seeder
{
    public function run()
    {
        $users = [
            [
                'email' => 'joni@gmail.com',
                'name' => 'Joni',
                'password' => Hash::make('password')
            ],
            [
                'email' => 'james@gmail.com',
                'name' => 'James',
                'password' => Hash::make('password')
            ],
            [
                'email' => 'lily@gmail.com',
                'name' => 'Lily',
                'password' => Hash::make('password')
            ]
        ];
        
        foreach($users as $u) {
            User::firstOrCreate(
                [ 'email' => $u['email']], $u
            );
        }
        
    }
}
