<?php

use Illuminate\Database\Seeder;
use App\Models\Acl\Permission;
use App\Models\Acl\Role;

class PermissionsSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            'assign-role-to-user',
            'remove-role-from-user',
            'give-permission',
            'revoke-permission',
            'ban-user',
            'unban-user',
            'edit-profile',
            'create-permission',
            'edit-permission',
            'delete-permission',
            'view-permission',
            'create-role',
            'edit-role',
            'delete-role',
            'view-role',
            'view-user'
        ];

        foreach ($permissions as $p) {
            Permission::firstOrCreate(['name' => $p]);
        }

        $role = Role::where('name', 'Super Admin')->first();
        if ($role) {
            $role->givePermissionTo($permissions);
        }
        
        $admin = Role::where('name', 'Admin')->first();
        if ($admin) {
            $admin->givePermissionTo(['view-user', 'view-permission', 'ban-user', 'view-role', 'assign-role-to-user', 'remove-role-from-user']);
        }
        
        $regular = Role::where('name', 'Regular')->first();
        if ($regular) {
            $regular->givePermissionTo(['edit-profile']);
        }
        
    }
}
