<?php

use Illuminate\Database\Seeder;
use App\Models\Acl\Role;
use App\User;

class RolesSeeder extends Seeder
{
    public function run()
    {
        $roles = [
            [
                'name' => 'Super Admin',
                'description' => 'Manage role and permission',
                'color' => 'dark'
            ],
            [
                'name' => 'Admin',
                'description' => 'Manage user in the system',
                'color' => 'danger'
            ],
            [
                'name' => 'Regular',
                'description' => 'This role will be given automatically after done email verification',
                'color' => 'light'
            ],
            [
                'name' => 'Vip',
                'description' => 'Normal user with extra abbilities',
                'color' => 'success'
            ],
            [
                'name' => 'Moderator',
                'description' => 'Promote user',
                'color' => 'primary'
            ]
        ];
        
        foreach($roles as $role) {
            Role::firstOrCreate(
                ['name' => $role['name']],
                $role
            );
        }
        
        $user = User::where('email', 'joni'.'@gmail.com')->first();
        if ($user) {
            $user->assignRole('Super Admin');
            $user->assignRole('Admin');
        }

        $admin = User::where('email', 'james'.'@gmail.com')->first();
        if ($admin) {
            $admin->assignRole('Admin');
        }

        $vip = User::where('email', 'lily'.'@gmail.com')->first();
        if ($vip) {
            $vip->assignRole('Vip');
        }
        
    }
}
