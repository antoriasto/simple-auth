<?php

use Illuminate\Database\Seeder;
use App\Models\Bookmark;
use App\User;

class BookmarksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$somaFm = new Bookmark();
    	$somaFm->title = 'Soma FM';
    	$somaFm->link = 'https://somafm.com/player/#/now-playing/indiepop';
    	$somaFm->description = 'Indie POP online radio';
    	$somaFm->rating = 5.0;
    	$somaFm->hidden = false;

    	$duplicate = Bookmark::where('title', 'Soma FM')->first();

        $user = User::where('email', 'joni'.'@gmail.com')->first();
             
        if ($user && !$duplicate) {
            $somaFm->user_id = $user->id;
            $somaFm->save();
        }
    }
}
