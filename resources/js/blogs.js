// alert('Dependency trigered for articles page');

// // Froala 
// var FroalaEditor = require('froala-editor');

// // Load a plugin.
// require('froala-editor/js/plugins/align.min');

// new FroalaEditor('#froala-editor', {
//     // Set maximum number of characters.
//     charCounterMax: 20
// }); 

var loading = true;

// require('@ckeditor/ckeditor5-build-classic/build/ckeditor.js');
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

ClassicEditor
    .create(document.querySelector('#editor'), {
    	toolbar: [ 'heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote' ],
        heading: {
            options: [
                { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
                { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' },
                { model: 'heading3', view: 'h3', title: 'Heading 3', class: 'ck-heading_heading3' },
            ]
        }
    })
    .then(editor => {
        window.editor = editor;
    })
    .catch(error => {
        console.error('There was a problem initializing the editor.', error);
    });

document.getElementById("myResetBtn").addEventListener("click", function(){
  document.getElementById("editor").innerHTML = "";
  window.editor.setData("");
}); 
window.Loading = loading;
// console.log('Dependency from blogs.js');
