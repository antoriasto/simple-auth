@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Article</div>

                <div class="card-body">
                    @if($posts->isEmpty())
                    There's no record to show ...
                    @else

                    <div class="my-1 p-1">
                        <h6 class="border-bottom pb-2 mb-0">{{ $listTitle }}</h6>
                        @foreach($posts as $p)
                        <div class="d-flex text-muted pt-3">
                            <span class="flex-shrink-0 mr-2" style="background-color: #bbb;   height: 25px;
  width: 25px; display: inline-block; border-radius: 50%;" width="32" height="32"></span>
                            <div class="d-flex border-bottom flex-fill">
                                <p class="pb-3 mb-0 small lh-sm">
                                    <strong class="d-block text-gray-dark">
                                        {{ $p->title }},
                                        <small class="text-muted">
                                        {{ $p->comments_count }} comments -
                                            {{ $p->created_at->format('M d, Y \a\t h:i a') }}
                                        </small>
                                    </strong>
                                    {{ Str::limit($p->body, $limit=50, $end=' ....' ) }}
                                    <a href="{{ route('blogs.show', $p->slug) }}">Read more</a>
                                </p>
                            </div>
                        </div>
                        @endforeach
                        
                        <small class="d-block text-right mt-3">
                            {{ $posts->links() }}
                        </small>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
