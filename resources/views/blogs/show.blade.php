@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header pt-0 pb-0">
                    <div class="mt-0 p-0 pb-0">
                        <div class="d-flex text-muted pt-3">

                            <span class="flex-shrink-0 mr-2" style="background-color: #bbb; height: 25px; width: 25px; display: inline-block; border-radius: 50%;" width="32" height="32">
                            </span>
                            <div class="d-flex flex-fill">

                                <p class="pb-3 mb-0 small lh-sm">
                                    <small>Created by, </small><strong>{{ $post->author->name }} </strong> <br>
                                 <small class="text-muted font-italic">Date created, {{ $post->created_at->format('M d, Y \a\t h:i a') }} </small> -
                                <small class="text-muted font-italic">Last update, {{ $post->created_at->format('M d, Y \a\t h:i a') }} </small>
                               
                            </p>
                        </div>
                        <div class="text-right">
                            @auth
                            @if(auth()->user()->id === $post->author_id )
                            <a href="{{ route('blogs.edit', $post->slug) }}" class="btn btn-sm btn-success text-right">Edit Post</a>
                            @endif
                            @endauth
                        </div>
                </div>
                        
                    </div>
                </div>
                <div class="card-body">
                    <h1> {{ $post->title }} </h1>
                    <hr>
                    {!! $post->body !!}
                </div>
            </div> <!-- post cards -->
            <div>
                @include('comments.list', ['post_id' => $post->id])
            </div> <!-- comment cards -->
        </div>
    </div>
</div>
@endsection
