@push('scripts')
<script src="{{ asset('js/blogs.js') }}" defer></script>
<script type="text/javascript">
</script>
@endpush

@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Article</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('blogs.update', $post->id) }}">
                        @csrf
                        @method('PATCH')
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name="title" class="form-control @error('name') is-invalid @enderror"  id="title" placeholder="Write title your article here" value="{{ old('title', $post->title) }}">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="body">Body</label>
                            <textarea id="editor" class="@error('body') is-invalid @enderror" name="body">
                                {!! old('body', $post->body) !!}
                            </textarea>
                            @error('body')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <div class="text-center">
                                <button type="submit" class="btn btn-success">
                                    {{ __('Submit') }}
                                </button> &nbsp;
                                <button type="reset" id="myBtn" class="btn btn-light">
                                    {{ __('Cancel') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div> <!--/cards -->
        </div> <!-- /column -->
    </div> <!-- /row container -->
</div> <!-- /container -->
@endsection
