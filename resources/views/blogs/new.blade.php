@push('scripts')
<script src="{{ asset('js/blogs.js') }}" defer></script>
@endpush

@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create New Article</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('blogs.store') }}"> 
                        @csrf
                        @method('POST')
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name="title" class="form-control @error('title') is-invalid @enderror" id="title" placeholder="Your title goes here ..." value="{{ old('title') }}">
                            @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="body">Body</label>
                            <textarea id="editor" name="body" class="@error('body') is-invalid @enderror" placeholder="Your post goes here ...">
                                {{ old('body') }}
                            </textarea>
                            @error('body')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <div class="text-center">
                                <button type="submit" class="btn btn-success">
                                    {{ __('Submit') }}
                                </button> &nbsp;
                                <button type="reset" id="myResetBtn" class="btn btn-light">
                                    {{ __('Cancel') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> <!-- /column -->
    </div> <!-- /row container --> 
</div> <!-- /container -->
@endsection