@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between">
                        Bookmarks
                        <div class="text-right">
                            <a href="{{ route('bookmarks.create') }}" class="btn btn-sm btn-success">Create New Bookmark</a>
                            <a href="{{ route('bookmarks.download', auth()->user()->id) }}" class="btn btn-sm btn-success">Export to JSON</a>
                            <a href="javascript:;" data-toggle="modal" data-target="#smallModal" class="btn btn-outline-dark btn-sm">Import JSON files</a>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class=" table table-sm table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th scope="col" style="width: 65%">Title</th>
                                    <th scope="col">URL</th>
                                    <th width="10" scope="col">Rating</th>
                                    <th width="10" scope="col">Hidden</th>
                                    <th width="10" scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($bookmarks as $bookmark)
                                <tr>
                                    <td>{{ $bookmark->title }}</td>
                                    <td>
                                        <a href="{{$bookmark->link}}" rel="noopener noreferrer" target="_blank">{{Str::limit($bookmark->link, $limit=25, $end=' ....' )}}</a>
                                    <td>
                                        <div class="rating p-0 m-0">
                                            @for ($i = 0; $i < $bookmark->rating; $i++)
                                            <i class="bi bi-star-fill"></i>
                                                <!-- <label class="rating" style="font-size: 1vw;">☆</label>  -->
                                            @endfor
                                        </div>
                                    </td>
                                    <td style="text-align: center;">
                                        @if($bookmark->hidden)
                                        <i class="bi bi-check"></i>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('bookmarks.edit', $bookmark->id) }}">Edit</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Upload Modal -->
<div class="modal fade" id="smallModal" tabindex="-1" role="dialog" aria-labelledby="smallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5>Import JSON File</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="smallBody">
                <div class="form-group">
                    <form action="{{ route('bookmarks.upload', auth()->user()->id) }}" method="POST" enctype="multipart/form-data" accept="json">
                            @csrf
                            @method('POST')
                        <div class="form-group">
                            <input type="file" class="form-control-file" id="exampleFormControlFile1" name="file">
                            <br>
                            <button class="btn btn-sm btn-primary">Upload</button>
                            <button class="btn btn-sm btn-light" data-dismiss="modal" aria-label="Close">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
 <!-- Modal -->

@endsection