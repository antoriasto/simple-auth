@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Bookmarks</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('bookmarks.update', $bookmarks->id) }}">
                        @csrf
                        @method('PATCH')
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name="title" class="form-control @error('title') is-invalid @enderror" id="title" rows="3" id="title" value="{{ $bookmarks->title }}">
                            @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="url">URL</label>
                            <input type="text" name="link" class="form-control @error('link') is-invalid @enderror" rows="3" id="url" value="{{ $bookmarks->link }}">
                            @error('link')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group p-0 m-0">
                            <label for="rating">Rating</label>
                            <div class="rating p-0 m-0" style="text-left;"> 
                                <input type="radio" name="rating" value="5" id="5" {{ ($bookmarks->rating == '5') ? 'checked' : '' }}>
                                <label for="5">☆</label> 
                                <input type="radio" name="rating" value="4" id="4" {{ ($bookmarks->rating == '4') ? 'checked' : '' }}>
                                <label for="4">☆</label> 
                                <input type="radio" name="rating" value="3" id="3" {{ ($bookmarks->rating == '3') ? 'checked' : '' }}>
                                <label for="3">☆</label> 
                                <input type="radio" name="rating" value="2" id="2" {{ ($bookmarks->rating == '2') ? 'checked' : '' }}>
                                <label for="2">☆</label> 
                                <input type="radio" name="rating" value="1" id="1" {{ ($bookmarks->rating == '1') ? 'checked' : '' }}>
                                <label for="1">☆</label>
                                </div>
                            </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea name="description" class="form-control @error('description') is-invalid @enderror" id="description" rows="3">{{ $bookmarks->description }}</textarea>
                            @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <div class="form-check">
                              <input class="form-check-input" type="checkbox" id="hidden" name="hidden" value="1"
                              {{ ($bookmarks->hidden) ? 'checked' : '' }}>
                              <label class="form-check-label" for="hidden">
                                Hidden
                              </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="d-flex">
                                <div class="mr-auto p-2">
                                <a href="javascript:;" data-toggle="modal" data-id='{{$bookmarks->id}}' data-target="#smallModal" class="btn btn-danger btn-sm">Delete</a>
                                </div>
                                <div class="p-2">
                                    <button class="btn btn-sm btn-success">Update</button>
                                </div>
                            </div>
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<!-- small modal -->
<div class="modal fade" id="smallModal" tabindex="-1" role="dialog" aria-labelledby="smallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="smallBody">
                <p>Are you sure want delete?</p>
                <div class="form-group">
                    <form method="POST" action="{{ route('bookmarks.delete', $bookmarks->id) }}">
                    @csrf
                    @method('DELETE')
                    </form>
                    <button class="btn btn-sm btn-danger">Yes</button>
                    <button class="btn btn-sm btn-light" data-dismiss="modal" aria-label="Close">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>
 <!-- Modal -->

 <script>
  console.log('Hola')
 </script>
@endsection