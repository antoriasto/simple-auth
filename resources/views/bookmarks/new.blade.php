@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Add New Bookmarks</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('bookmarks.store') }}">
                        @csrf
                        @method('POST')
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name="title" class="form-control @error('title') is-invalid @enderror" id="title" rows="3" id="title" value="{{ old('title') }}">
                            @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="url">URL</label>
                            <input type="text" name="link" class="form-control @error('link') is-invalid @enderror" rows="3" id="url" value="{{ old('link') }}">
                            @error('link')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group p-0 m-0">
                            <label for="rating">Rating</label>
                            <div class="rating p-0 m-0" style="text-left;"> 
                                <input type="radio" name="rating" value="5" id="5">
                                <label for="5">☆</label> 
                                <input type="radio" name="rating" value="4" id="4">
                                <label for="4">☆</label> 
                                <input type="radio" name="rating" value="3" id="3">
                                <label for="3">☆</label> 
                                <input type="radio" name="rating" value="2" id="2">
                                <label for="2">☆</label> 
                                <input type="radio" name="rating" value="1" id="1" checked="checked">
                                <label for="1">☆</label>
                                </div>
                            </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea name="description" class="form-control @error('description') is-invalid @enderror" id="description" rows="3"></textarea>
                            @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <div class="form-check">
                              <input class="form-check-input" type="checkbox" id="hidden" name="hidden" value="1">
                              <label class="form-check-label" for="hidden">
                                Hidden
                              </label>
                            </div>
                        </div>
                        <div class="form-group text-right">
                            <button class="btn btn-sm btn-success">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection