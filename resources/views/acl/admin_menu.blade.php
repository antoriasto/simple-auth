{{-- <ul class="navbar-nav mr-auto"> --}}
    <li class="nav-item dropdown {{ (request()->is('acl/*')) ? 'active' : '' }}">
        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
            Admin <span class="caret"></span>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
            <a class="dropdown-item {{ (request()->is('acl/users*')) ? 'active' : '' }}" href="{{ route('users.index') }}">
                Users
            </a>
            <a class="dropdown-item {{ (request()->is('acl/roles*')) ? 'active' : '' }}" href="{{ route('roles.index') }}">
                Roles
            </a>
            <a class="dropdown-item {{ (request()->is('acl/permissions*')) ? 'active' : '' }}" href="{{ route('permissions.index') }}">
                Permissions
            </a>
        </div>
    </li>
{{-- </ul> --}}