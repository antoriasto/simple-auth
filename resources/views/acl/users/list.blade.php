@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"></div>
                <div class="card-body">
                    <div>
                        <h3>Users</h3>
                        <p>This pages contains list of users</p>
                    </div>
                    <hr/>
                    <div class="table-responsive">
                        <table class=" table table-sm table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th width="30">Verified emails</th>
                                    <th>Roles</th>
                                    <th width="90"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $key => $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td class="text-center">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="chk[{{$user->id}}]" <?php if (isset($user->email_verified_at)) echo 'checked' ?> disabled />
                                            <label class="custom-control-label" for="chk[{{$user->id}}]"></label>
                                        </div>
                                    </td>
                                    <td>
                                        @if( count($user->roles) > 0 )
                                        @foreach( $user->roles as $role )
                                        <span class="badge badge-pill badge-{{$role->color}}">{{ $role->name }}</span>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        <a role="button" class="btn btn-outline-dark btn-sm btn-block" href="{{ route('users.roles', $user->id) }}">
                                            {{ __('Edit Roles') }}
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $users->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection