@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><a class="btn btn-sm btn-outline-dark" role="buton" href="{{ route('users.index')}}">Back</a></div>
                <div class="card-body">
                    <div>
                        <h3>Editing Roles User</h3>
                        <p>This pages contains for editing roles user</p>
                    </div>
                    <hr />
                    <div class="d-flex justify-content-center">
                        <div class="card text-center" style="width: 25rem;">
                            <div class="card-body">
                                <div class="text-center">
                                    <label class="text-muted">@username: <b>{{$user->name}}</b></label> &nbsp;
                                    <label class="text-muted">@email: <b>{{$user->email}}</b></label>
                                </div>
                                <div class="text-center">
                                    @if ($rolesUser->isEmpty())
                                        This user don't have any roles
                                    @endif
                                    @if ($rolesUser->isNotEmpty())
                                        @foreach( $rolesUser as $role )
                                        <span class="badge badge-pill badge-warning">{{ $role }}</span>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="d-flex justify-content-center">
                        <div class="col-lg-10 table-responsive mt-3">
                            <table class=" table table-sm table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($roles as $key => $role)
                                    <tr>
                                        <td>{{ $role->id }}</td>
                                        <td>{{ $role->name }}</td>
                                        <td>{{ $role->description }}</td>
                                        <td>
                                            @if($role->status == false)
                                            <form method="POST" action="{{ route('users.roles.add', ['userId' => $user->id]) }}">
                                                @csrf
                                                @method('POST')
                                                <input type="hidden" name="role" value="{{ $role->id }}" />
                                                <button type="submit" class="btn btn-success btn-sm btn-block">
                                                    {{ __('Assign') }}
                                                </button>
                                            </form>
                                            @endif
                                            @if($role->status == true)
                                            <form method="POST" action="{{ route('users.roles.remove', ['userId' => $user->id]) }}">
                                                @csrf
                                                @method('DELETE')
                                                <input type="hidden" name="role" value="{{ $role->id }}" />
                                                <button type="submit" class="btn btn-danger btn-sm btn-block">
                                                    {{ __('Revoke') }}
                                                </button>
                                            </form>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection