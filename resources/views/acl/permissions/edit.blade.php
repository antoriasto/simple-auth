@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><a class="btn btn-sm btn-outline-dark" href="{{ route('permissions.index') }}">Back</a></div>
                <div class="card-body">
                    <h3>Edit Permissions</h3>
                    <p>This pages contains for editing spesifict permissions</p>
                    <hr />
                    <form method="POST" action="{{ route('permissions.update', ['permission' => $permissions['id'] ]) }}">
                        @csrf
                        @method('PATCH')
                        <div class="form-group row">
                            <label for="id" class="col-md-4 col-form-label text-md-right">{{ __('ID') }}</label>
                            <div class="col-md-6">
                                <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{ $permissions['id'] }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old ('name', $permissions['name']) }}" required autocomplete="name" autofocus>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="description" value="{{ old('description', $permissions['description']) }}" required>
                                @error('description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="lastupdate" class="col-md-4 col-form-label text-md-right">{{ __('Last update') }}</label>
                            <div class="col-md-6">
                                <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{ $permissions['last_update'] }}">
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Update') }}
                                </button>
                                &nbsp;
                                <button type="reset" class="btn btn-outline-dark">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
