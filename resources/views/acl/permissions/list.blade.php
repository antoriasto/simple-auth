@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="text-right">
                        <a role="button" class="btn btn-sm btn-outline-dark" href="{{ route('permissions.create') }}">Create New</a>
                    </div>
                </div>
                <div class="card-body">
                    <h3 class="card-tittle">Permissions</h3>
                    <p class="card-text">This pages contains list of permission</p>
                    <hr />
                    @if ( $permissions->isNotEmpty() )
                    <div class="table-responsive">
                        <table class=" table table-sm table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Last Update</th>
                                    <th width="50"></th>
                                    <th width="50"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($permissions as $key => $p)
                                <tr>
                                    <td>{{ $p['id'] }}</td>
                                    <td>{{ $p['name'] }}</td>
                                    <td>{{ $p['last_update'] }}</td>
                                    <td>
                                        <a role="button" class="btn btn-sm btn-info" href="{{ route('permissions.edit', ['permission' => $p['id'] ]) }}">Edit</a>
                                    </td>
                                    <td>
                                        <form method="POST" action="{{ route('permissions.destroy', ['permission' => $p['id'] ]) }}">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger btn-sm btn-block">
                                                {{ __('Delete') }}
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $permissions->links() }}
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
