@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <a class="btn btn-sm btn-outline-dark" href="{{ route('roles.index') }}">Back</a>
                </div>
                <div class="card-body">
                    <div class="mb-3">
                        <h3>Edit Role</h3>
                        <p>This pages contains for editing spesifict roles</p>
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="{{ (request()->is('acl/roles/*/edit')) ? 'nav-link active' : 'nav-link' }}" href="{{ route('roles.edit', $role->id) }}" role="tab" aria-controls="home" aria-selected="true">Details</a>
                            </li>
                            <li class="nav-item">
                                <a class="{{ (request()->is('acl/roles/*/permissions')) ? 'nav-link active' : 'nav-link' }}" href="{{ route('roles.permissions', $role->id) }}" role="tab" aria-controls="profile" aria-selected="false">Permissions ({{$totalPermsThisRole}})</a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content">
                        @yield('edit-content')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function validateDelete() {
        if (!confirm('Are you sure want to delete this?')) {
            event.preventDefault();
        }
    }
</script>
@endsection