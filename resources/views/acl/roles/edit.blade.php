@extends('acl.roles.layout')

@section('edit-content')
<div class="tab-pane active" id="role-details">
    <form method="POST" action="{{ route('roles.update', $role->id) }}">
        @csrf
        @method('PUT')
        <div class="form-group row">
            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('ID') }}</label>

            <div class="col-md-6">
                <input id="name" type="text" class="form-control @error('id') is-invalid @enderror" name="id" value="{{ $role->id }}" required />

                @error('id')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="form-group row">
            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

            <div class="col-md-6">
                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $role->name }}" required autocomplete="name" />

                @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="form-group row">
            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Color') }}</label>
            <div class="col-md-6">
                <select class="form-control @error('color') is-invalid @enderror" name="color" value="{{$role->color}}" id="exampleFormControlSelect1">
                <option></option>
                @foreach($colors as $key => $c)
                <option value="{{$key}}" {{ ( $key === $role->color ) ? 'selected' : '' }} >
                {{ $c }}
                </option>
                @endforeach
                </select>
                @error('color')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="form-group row">
            <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>

            <div class="col-md-6">
                <textarea id="description" class="form-control @error('description') is-invalid @enderror" name="description" rows="4" required>{{$role->description}}</textarea>
                @error('description')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <hr />
        <div class="form-group row">
            <label for="name" class="col-md-4 col-form-label text-md-right"></label>

            <div class="col-md-6">
                <button type="submit" class="btn btn-success">Update</button> &nbsp;
                <button type="reset" class="btn btn-outline-dark">Cancel</button>
            </div>
        </div>
    </form>
</div>
<!--</div>-->

@endsection