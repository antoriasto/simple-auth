@extends('acl.roles.layout')

@section('edit-content')
<div class="tab-pane active" id="role-permissions">
    <div class="form-group row">
        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Role Name') }}</label>
        <div class="col-md-6">
            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $role->name }}" required autocomplete="name" />
        </div>
    </div>
    <div class="form-group row">
        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Permission') }}</label>
        <div class="col-md-7">
            <div class="table-responsive">
                <table class=" table table-sm table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Name</th>
                            @can('edit-permission')
                            <th class="text-center">
                                Status
                            </th>
                            <th>
                                &nbsp;
                            </th>
                            @endcan
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($permissions as $key => $permission)
                        <tr>
                            <td>{{ $permission->name }}</td>
                            @can('edit-permission')
                            <td class="text-center">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="chk[{{$permission->id}}]" <?php if ($permission->status) echo 'checked' ?> disabled>
                                    <label class="custom-control-label" for="chk[{{$permission->id}}]"></label>
                                </div>
                            </td>
                            <td class="text-center">
                                @if($permission->status == false)
                                <form method="POST" action="{{ route('roles.permissions.add', ['roleId' => $role->id]) }}">
                                    @csrf
                                    @method('POST')
                                    <input type="hidden" name="permission" value="{{ $permission->id }}" />
                                    <button type="submit" class="btn btn-success btn-sm btn-block">
                                        {{ __('Give') }}
                                    </button>
                                </form>
                                @endif
                                @if($permission->status == true)
                                <form method="POST" action="{{ route('roles.permissions.remove', ['roleId' => $role->id]) }}">
                                    @csrf
                                    @method('DELETE')
                                    <input type="hidden" name="permission" value="{{ $permission->id }}" />
                                    <button type="submit" class="btn btn-danger btn-sm btn-block">
                                        {{ __('Remove') }}
                                    </button>
                                </form>
                                @endif
                            </td>
                            @endcan
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $permissions->links() }}
            </div>
        </div>
    </div>
</div>

@endsection