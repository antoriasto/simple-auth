@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="text-right">
                        <a role="button" class="btn btn-sm btn-outline-dark" href="{{ route('roles.create') }}">Create New</a>
                    </div>
                </div>
                <div class="card-body">
                    <h3 class="card-tittle">Roles</h3>
                    <p class="card-text">This pages contains list of role</p> <hr/>
                    @foreach($roles->chunk(3) as $chunk)
                    <div class="row">
                        @foreach($chunk as $role)
                        <div class="col-md-4 col-sm-12 mb-1">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">{{ $role->name }} <span class="text-danger">*</span></h5> 
                                <p class="card-text">{{ $role->description }}</p>
                                <a href="{{ route( 'roles.edit',  $role->id) }}" class="btn btn-info btn-sm text-white" role="button">View Details</a>
                            </div>
                            <div class="card-footer">
                                <small class="text-muted">Last updated {{ Carbon\Carbon::parse($role->updated_at)->diffForHumans()}} </small>
                            </div>
                        </div>
                        </div>
                        @endforeach
                    </div>
                    <br>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection