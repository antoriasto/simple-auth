<div class="card mt-3">
    <div class="card-header pt-1 pb-1">
        Comments <span class="badge badge-secondary">{{ count($comments) }}</span>
    </div>
    <div class="card-body">
        @if($comments->isNotEmpty())
        @foreach($comments as $c)
        <div class="alert alert-light border-dark pt-0 pr-1 pl-1 pb-0" role="alert">
            <div class="container">
                <div class="d-flex justify-content-between border-bottom">
                    <p>
                        <small class="text-muted"><strong>{{ $c->user->name }}</strong>, says </small>
                    </p>
                    @auth
                    @if($owner)
                    <div class="p-0">
                        <form action="{{ route('comments.disable', $c->id ) }}" method="POST">
                            @csrf
                            @method('PATCH')
                            <button type="submit" role="button" class="badge badge-danger">Disable</button>
                        </form>
                    </div>
                    @endif
                    @endauth
                </div>
                <p class="mt-1">{{ $c->body }}</p>
                <p class="border-top text-right pb-0 mb-0 font-italic text-muted">
                    <small>{{ $c->updated_at->format('M d, Y \a\t h:i a') }}</small>
                </p>
            </div>
        </div>
        @endforeach
        @endif
        @auth
        @include('comments.new', ['some' => 'data'])
        @endauth
    </div>
</div>
