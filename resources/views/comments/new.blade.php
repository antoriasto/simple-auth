<form method="POST" action="{{ route('comments.store') }}">
    @csrf
    @method('POST')
    <div class="form-group">
        <label for="exampleFormControlTextarea1">Write your comment</label>
        <textarea name="body" class="form-control @error('body') is-invalid @enderror" id="exampleFormControlTextarea1" rows="3"></textarea>
        @error('body')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <input type="hidden" name="userId" value="{{ auth()->user()->id }}">
    <input type="hidden" name="postId" value="{{ $post->id }}">
    <div class="form-group text-right">
        <button class="btn btn-sm btn-success">Post</button>
    </div>
</form>
