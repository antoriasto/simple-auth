<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    
    protected $fillable = [
    	'first_name', 'last_name', 'address', 'phone', 'user_id'
    ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
