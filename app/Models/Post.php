<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';

    protected $fillable = [
    	'title', 'body', 'slug', 'active', 'published', 'active'
    ];

  	public function author()
  	{
    	return $this->belongsTo('App\User', 'author_id');
  	}

}
