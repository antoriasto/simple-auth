<?php

namespace App\Libraries;

class Color
{
    private static $colors = array(
            'dark' => 'Black',
            'primary' => 'Blue Dark',
            'warning' => 'Yellow',
            'info' => 'Blue',
            'danger' => 'Red',
            'success' => 'Green',
            'light' => 'White',
            'secondary' => 'Gray'
        );
    
    public static function getColors() {
        return self::$colors;
    }
}
