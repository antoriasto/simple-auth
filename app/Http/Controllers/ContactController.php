<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactStoreRequest;
use App\Models\Contact;

class ContactController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

	public function update(ContactStoreRequest $request)
	{
		$contact = Contact::where('user_id', auth()->user()->id)->firstOrFail();
		
		$contact->first_name = $request->firstName;
		$contact->last_name = $request->lastName;
		$contact->address = $request->address;
		$contact->phone = $request->phone;

		$isSaved = $contact->save();
		if ($isSaved) {
            return redirect()->route('profiles.index')->with(['success' => 'Your profile has been updated']);
        }
        return redirect()->back()->with(['error'=> 'Failed updating profile']);
	}

    public function index()
    {	
    	// Getting current user login
    	$user = auth()->user();

    	// Validate the profile record
    	$this->checkIfContactNotPresent($user->id);

    	$contact = Contact::where('user_id', $user->id)->first();

    	return view('profile.contact', [
    		'contact' => $contact
    	]);
    }

    private function checkIfContactNotPresent($userId) 
    {
    	$contact = Contact::where('user_id', $userId)->first();
    	if (!$contact)
    	{
    		// create new profile
    		Contact::create([
    			'user_id' => $userId
    		]);
    	}
    }

}
