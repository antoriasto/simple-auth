<?php

namespace App\Http\Controllers;

use App\Models\Bookmark;
use Illuminate\Http\Request;
use App\Http\Requests\BookmarkStoreRequest;

class BookmarkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Getting current user login
        $user = auth()->user();

        $bookmarks = Bookmark::paginate(10)->where('user_id', $user->id);

        return view('bookmarks.list', [
            'bookmarks' => $bookmarks
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('bookmarks.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookmarkStoreRequest $request)
    {
        $bookmark = new Bookmark();
        
        $bookmark->title = $request->get('title');
        $bookmark->link = $request->get('link');
        $bookmark->rating = $request->get('rating');
        $bookmark->description = $request->get('description');
        $bookmark->hidden = $request->get('hidden') ? 1 : 0;
        $bookmark->user_id = auth()->user()->id;

        $isSaved = $bookmark->save();

        if ($isSaved) {
            return redirect()->route('bookmarks')->with(['success' => 'Bookmark has been created']);
        }

        return redirect()->back()->with(['error'=> 'Failed create bookmark']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bookmark  $bookmark
     * @return \Illuminate\Http\Response
     */
    public function show(Bookmark $bookmark)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bookmark  $bookmark
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Bookmark::findOrFail($id);

        $bookmark = Bookmark::where('id', $id)->first();

        return view('bookmarks.edit', [
            'bookmarks' => $bookmark
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bookmark  $bookmark
     * @return \Illuminate\Http\Response
     */
    public function update(BookmarkStoreRequest $request, $id)
    {
        $bookmark = Bookmark::findOrFail($id);

        $isSaved = $bookmark->update($request->all());

        if ($isSaved) {
            return redirect()->back()->with(['success'=> 'Bookmarks has been updated']);
        }

        return redirect()->back()->with(['error'=> 'Failed updating Bookmarks']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bookmark  $bookmark
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Bookmark::findOrFail($id);

        $bookmark = Bookmark::where('id', $id)->first();

        $isDeleted = $bookmark->delete();

        if ($isDeleted) {
            return redirect()->route('bookmarks')->with(['success' => 'Bookmark has been deleted']);
        }

        return redirect()->back()->with(['error'=> 'Failed updating Bookmarks']);
    }

    public function exportToJSON($id) 
    {
        Bookmark::findOrFail($id);

        $table = Bookmark::where('user_id', $id)->get();

        $filename = "bookmarks.json";

        $handle = fopen($filename, 'w+');

        fputs($handle, $table->toJson(JSON_PRETTY_PRINT));

        fclose($handle);

        $headers = array('Content-type'=> 'application/json');

        return response()->download($filename,'bookmarks.json',$headers);
    }

    public function importJSON(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:csv,txt,json|max:2048'
        ]);

        $file = $request->file;

        $json =  file_get_contents($file->getRealPath());

        $bookmarks = json_decode($json, true);

        if (json_last_error() === JSON_ERROR_NONE) 
        {
            // JSON is valid
            foreach ($bookmarks as $k => $v) {
                Bookmark::updateOrcreate($v);
            }

            return redirect()->route('bookmarks')->with(['success' => 'Bookmark has been updated']);
        }

        return redirect()->back()->with(['error'=> 'Failed updating Bookmarks']);
    }
}
