<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Rules\MatchOldPassword;
use App\User;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index()
	{
		return view('auth.passwords.change');	
	}

    public function store(Request $request)
    {
    	$request->validate([
            'password' => ['required', new MatchOldPassword],
            'newPassword' => ['required'],
            'newConfirmPassword' => ['same:newPassword'],
        ]);

        $isSaved = User::find(auth()->user()->id)->update(['password'=> Hash::make($request->newPassword)]);

        if ($isSaved) {
            return redirect()->back()->with(['success' => 'Your credentials has been changed']);
        }
        return redirect()->back()->with(['error'=> 'Failed updating your credentials']);

    }
}
