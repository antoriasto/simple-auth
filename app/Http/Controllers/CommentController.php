<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CommentStoreRequest;
use App\Models\Comment;
use App\Models\Post;

class CommentController extends Controller
{
    public function store(CommentStoreRequest $request)
    {
    	$post = Post::findOrFail($request->postId);

    	$comment = new Comment();
    	$comment->body = $request->body;
    	$comment->user_id = $request->userId;
    	$comment->post_id = $request->postId;

    	$isSaved = $comment->save();
    	if ($isSaved) {
    		return redirect()->route('blogs.show', ['slug' => $post->slug])->with(['success'=> 'Your comments has been created']);
    	}
        return redirect()->back()->with(['error'=> 'Failed create comments']);
    }

    public function disableComments($id)
    {
        $comments = Comment::findOrFail($id);
        $posts = Post::findOrFail($comments->post_id);
        
        // validate if request true from the owner post
        if (auth()->user()->id === $posts->id) {
            $comments->banned = true;
            $comments->save();
            return redirect()->route('blogs.show', ['slug' => $posts->slug])->with(['success'=> 'Comments has been disable']);
        }

        return redirect()->back()->with(['error'=> 'Failed disable comments']);
    
    }

}
