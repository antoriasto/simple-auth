<?php

namespace App\Http\Controllers\Acl;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\PermissionRepository ;
use App\Http\Requests\PermissionStoreRequest;

class PermissionController extends Controller
{

    private $permissionRepo;

    public function __construct(PermissionRepository $permissionRepo) 
    {
        $this->permissionRepo = $permissionRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('acl.permissions.list', [
                'permissions' => $this->permissionRepo->getAll()
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('acl.permissions.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PermissionStoreRequest $request)
    {
        $validated = $request->validated();
        
        $isSaved = $this->permissionRepo->save($request);
        
        if ($isSaved) {
            return redirect()->back()->with(['success'=> 'New permissions has been created']);
        }
        return redirect()->back()->with(['error'=> 'Failed create new Permissions']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($permissionId)
    {
        $permissions = $this->permissionRepo->findById($permissionId);
        return $permissions;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permissions = $this->permissionRepo->findById($id);
        return view('acl.permissions.edit')->with(['permissions' => $permissions]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PermissionStoreRequest $request, $id)
    {
        $isUpdated = $this->permissionRepo->update($request->all(), $id);

        if ($isUpdated) {
            return redirect()->back()->with(['success'=> 'Permissions has been updated']);
        }
        return redirect()->back()->with(['error'=> 'Failed updating Permissions']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($permissionId)
    {
        $isDeleted = $this->permissionRepo->delete($permissionId);
        if ($isDeleted) {
            return redirect()->back()->with(['success'=> 'Permissions has been deleted']);
        }
        return redirect()->back()->with(['error'=> 'Failed deleted permissions']);
    }
}
