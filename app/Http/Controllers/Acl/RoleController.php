<?php

namespace App\Http\Controllers\Acl;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Acl\Role;
use App\Models\Acl\Permission;
use Carbon\Carbon;
use App\Libraries\Color;
use App\Http\Requests\RoleStoreRequest;
use App\Repositories\RoleRepository;

class RoleController extends Controller {
    
    private $roleRepo;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepo = $roleRepository;
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index() 
    {
        $roles = Role::paginate( 15 );
        return view('acl.roles.list', ['roles' => $roles ]);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create() 
    {
        return view('acl.roles.new', [
            'colors' => Color::getColors()
        ]);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(RoleStoreRequest $request) 
    {
        $isSaved = $this->roleRepo->save($request);
        if ($isSaved) {
            return redirect()->route('roles.index')->with(['success' => 'Role has been created']);
        }
        return redirect()->back()->with(['error'=> 'Failed created new role']);
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show( $id ) {
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit( $id ) {
        
        // Validate role
        $role = Role::findOrFail($id);
        
        // Source permissions data
        $permissions = Permission::paginate(10);
        // Get all permission by role id
        $rolePermissions = $role->permissions()->get();

        return view('acl.roles.edit', [
            'role' => $role,
            'permissions' => $permissions,
            'totalPermsThisRole' => count($rolePermissions),
            'colors' => Color::getColors()
        ]);
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function showRolePermissions( $id ) {
        
        // Validate role
        $role = Role::findOrFail($id);
        
        // Source data
        $permissions = Permission::paginate(10);
        $rolePermissions = $role->permissions()->get();
        
        // Add extra column for view
        $permissions->map(function ($p){
            $p['status'] = false;        
            return $p;
        });
        
        // Sorting
        foreach ($permissions as $p) {
            if ( count($rolePermissions->where('name', $p->name)) > 0 ) {
                $p['status'] = true;
            }
        }
        
        return view('acl.roles.permissions', [
            'role' => $role,
            'permissions' => $permissions,
            'totalPermsThisRole' => count($rolePermissions)
        ]);
    }
    
    public function removingPermissionFromRole(Request $request, $roleId) {
        
        // Validate role
        $role = Role::findOrFail($roleId);
        
        // Validate permission
        $permission = Permission::findOrFail($request->permission);
        
        
        // Validate if current role have this permission and removing
        if ($role->hasPermissionTo($permission)) {
            if ($role->revokePermissionTo($permission)) {
                
                // Update role parent last update
                $role->updated_at = Carbon::now();
                $role->save(['timestamps' => FALSE]);
                
                return redirect()->back()->with(['success'=> 'Role has been updated']);
            }
        }
        
        return redirect()->route('roles.permissions.edit', $id);
    }
    
    public function addingPermissionToRole(Request $request, $roleId) {
        
        // Validate role
        $role = Role::findOrFail($roleId);
        
        // Validate permission
        $permission = Permission::findOrFail($request->permission);
        
        // Adding permission to role
        if ($role->givePermissionTo($request->permission)) {
            
            // Update role parent last update
            $role->updated_at = Carbon::now();
            $role->save(['timestamps' => FALSE]);
            
            return redirect()->back()->with(['success'=> 'Role has been updated']);
        }
        
        return redirect()->route('roles.permissions.edit', $id);
    }
    
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update( Request $request, $roleId ) {
        
        // Validate form request
        $validatedData = $request->validate([
            'name' => 'required|max:50|min:2',
            'description' => 'required|max:200|min:10',
            'color' => 'required'
        ]);
        
        // Validate role
        $role = Role::findOrFail($roleId);
        
        $update = $role->update([
            'name' => $request->name,
            'description' => $request->description,
            'color' => $request->color
        ]);
        
        if ($update) {
            return redirect()->back()->with(['success'=> 'Role has been updated']);
        }
        
        return redirect()->back()->with(['error'=> 'Failed updating role']);
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy( $id ) {
        //
    }
}