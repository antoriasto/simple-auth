<?php

namespace App\Http\Controllers\Acl;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Acl\Role;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get users except current user login
        $users = User::where('id', '!=', auth()->id())->paginate(10);

        return view('acl.users.list', compact('users'));
    }
    
    public function editingRolesUser($userId) {
        
        // Validate user
        $user = User::findOrFail($userId);
        
        // Get roles user
        $rolesUser = $user->getRoleNames();
        
        // Get roles for option
        $roles = Role::paginate(20);
        
        // Add extra column for view
        $roles->map(function ($r){
            $r['status'] = false;        
            return $r;
        });

        // Sorting
        foreach ($roles as $r) {
            foreach($rolesUser as $ru) {
                if ($r['name'] === $ru) {
                    $r['status'] = true;
                    break;
                }
            }
        }
        
        return view('acl.users.roles', [
            'user' => $user,
            'rolesUser' => $rolesUser,
            'roles' => $roles
        ]);
    }
    
    public function addingRoleToUser(Request $request, $userId) {
        $user = User::findOrFail($userId);
        $role = Role::findOrFail($request->role);
        if ($user->assignRole($role)) {
            return redirect()->back()->with(['success'=> 'Roles user has been updated']);
        }
        return redirect()->back()->with(['error'=> 'Failed updating roles user']);
    }
    
    public function removingRoleFromUser(Request $request, $userId) {
        $user = User::findOrFail($userId);
        $role = Role::findOrFail($request->role);
        if ($user->removeRole($role)) {
            return redirect()->back()->with(['success'=> 'Roles user has been updated']);
        }
        return redirect()->back()->with(['error'=> 'Failed updating roles user']);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
