<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PostStoreRequest;
use App\Http\Requests\PostUpdateRequest;
use App\Models\Post;
use App\Models\Comment;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{
    public function index()
    {
        $recentPosts = $this->getRecentPost(5);
        
        $mostComments = Post::select(['slug', 'body', 'updated_at', 'created_at', 'title'])
            ->with('author')
            ->addSelect(['comments_count' => function($query) {
                $query->selectRaw('count(*) as comments_count')
                ->from('comments')
                ->whereColumn('posts.id', 'comments.post_id');
            }])
            ->whereRaw('(select count(*) from comments where posts.id = comments.post_id) > 0')
            ->orderBy('comments_count', 'desc')
            // for mysql using having (this code above) for postgresql using whereraw
            // ->having('comments_count', '>', 0)
            ->limit(5)
            ->get();

    	return view('blogs.dashboard',[
            'posts' => $recentPosts,
            'mostComments' => $mostComments
        ]);
    }

    public function newPost()
    {
    	return view('blogs.new');
    }

    public function store(PostStoreRequest $request)
    {
    	$post = new Post();
        $post->title = $request->get('title');
        $post->body = $request->get('body');
        $post->slug = Str::slug($post->title);
        $post->author_id = auth()->user()->id;
        $post->active = true;

        $isSaved = $post->save();
        if ($isSaved) {
            return redirect()->route('blogs')->with(['success' => 'Post has been created']);
        }
        return redirect()->back()->with(['error'=> 'Failed create post']);

    }

    public function show($slug)
    {
        $post = Post::where('slug', $slug)->first();
        $comments = Comment::where([
            ['post_id', $post->id],
            ['banned', false],
        ])->with('user')->paginate(15);

        $owner = false;
        if ( auth()->user() ) {
            $owner = auth()->user()->id === $post->author_id ? true : false;
        }

        if ($post) {
            return view('blogs.show', [
                'post' => $post,
                'comments' => $comments,
                'owner' => $owner
            ]);
        }
    }

    public function edit($slug)
    {
        $post = Post::where('slug', $slug)->firstOrFail();
        // Validate if current user is truly who create the post
        if ( $post->author_id === auth()->user()->id ) {
            return view('blogs.edit',[
                'post' => $post
            ]);
        }
        return redirect()->back()->with(['error'=> 'Article not found']);
    }

    public function update(PostUpdateRequest $request, $postId)
    {
        $post = Post::findOrFail($postId);
        if ( $post->author_id !== auth()->user()->id ) {
            return redirect()->back()->with(['error'=> 'You not allowed to update this Post']);
        }

        $post->title = $request->get('title');
        $post->body = $request->get('body');
        $post->slug = Str::slug($post->title);
        $isSaved = $post->save();
        
        if ($isSaved) {
            return redirect()->route('blogs.show', ['slug' => $post->slug])->with(['success'=> 'Your post has been updated']);
        }

        return redirect()->back()->with(['error'=> 'Failed updating post']);
    }

    public function articleList(Request $request)
    {
        $sort = $request->query('sort');
        $posts = null;
        $listTitle = '';

        switch ($sort) {
            case 'most_comments':
                $listTitle = 'Most comments';
                $posts = $this->getMostComments();
                break;
            default:
                $listTitle = 'Recent post';
                $posts = $this->getRecentPost();
                break;
        }

        return view('blogs.list',[
            'posts' => $posts,
            'listTitle' => $listTitle
        ]);
    }

    private function getRecentPost($limit = 15)
    {
        return Post::select(['slug', 'body', 'updated_at', 'created_at', 'title'])
            ->with('author')
            ->addSelect(['comments_count' => function($query) {
                $query->selectRaw('count(*) as comments_count')
                ->from('comments')
                ->whereColumn('posts.id', 'comments.post_id');
            }])
            ->orderBy('created_at', 'desc')
            ->paginate($limit);
    }

    private function getMostComments()
    {
        return Post::select(['slug', 'body', 'updated_at', 'created_at', 'title'])
            ->with('author')
            ->addSelect(['comments_count' => function($query) {
                $query->selectRaw('count(*) as comments_count')
                ->from('comments')
                ->whereColumn('posts.id', 'comments.post_id');
            }])
            ->orderBy('comments_count', 'desc')
            ->paginate(15);
    }

}
