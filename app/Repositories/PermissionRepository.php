<?php

namespace App\Repositories; 

use App\Models\Acl\Permission;

class PermissionRepository 
{
	public function getAll() 
	{
		$permissions = Permission::paginate(15);
		
		$permissions->getCollection()->transform(function($item, $key) {
            return $this->format($item);
        });

		return $permissions;
	}

	public function findById($id)
	{
		$permission = Permission::where('id', $id)->firstOrFail();
		return $this->format($permission);
	}

	private function format($permission) 
	{
		return [
			'id' => $permission->id,
			'name' => $permission->name,
			'color' => $permission->color,
			'description' => $permission->description,
			'last_update' => $permission->updated_at->diffForHumans()
		];
	}

	public function save($data) 
	{
		$permissionName = $data->name;
		$permissionDescription = $data->description;

		return Permission::create([
			'name' => $permissionName,
			'description' => $permissionDescription
		]);
	}

	public function delete($permissionId)
	{
		$perms = Permission::findOrFail($permissionId);
        return $perms->delete();
	}

	public function update($data, $permissionId)
	{
		$perms = Permission::findOrFail($permissionId);

		$perms->name = $data["name"];
		$perms->description = $data["description"]; 
        $perms->save();

        return $perms;
	}
}
