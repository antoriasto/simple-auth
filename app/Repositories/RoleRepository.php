<?php

namespace App\Repositories;

use App\Models\Acl\Role;

/**
 * 
 */
class RoleRepository
{
	
	public function save($data)
	{	
		$roleName = $data->name;
        $roleDescription = $data->description;
        $roleColor = $data->color;

		return Role::create([
            'name' => ucfirst($roleName),
            'description' => $roleDescription,
            'color' => $roleColor
        ]);
	}

	public function delete()
	{
		$role = Role::findOrFail($id);
        if ($role->name !== 'Admin' || $role->name !== 'Owner') {
            $role->delete();
        } else {
            Session()->flash('flash_message_warning', 'Can not delete Administrator role');
        }
	}

}