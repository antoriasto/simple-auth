<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Index
Route::get('/', function () { return view('welcome'); });

Auth::routes(['verify' => true]);

Route::group(['middleware' => ['auth']], function () {
	Route::get('/home', 'HomeController@index')->name('home');

	// Custom routes for email
	// Route::get('email/verify/{id}/{hash}', 'Auth\VerificationController@verify')->name('verification.verify')->middleware('signed');
	// Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
	// Route::post('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');

	Route::get('/change-password', 'Auth\ChangePasswordController@index' )->name('password.index');
	Route::post('/change-password', 'Auth\ChangePasswordController@store' )->name('password.store');

	Route::resources([
	    'acl/roles' => 'Acl\RoleController',
	    'acl/permissions' => 'Acl\PermissionController',
	    'acl/users' => 'Acl\UserController'
	]);
	Route::get('/acl/roles/{id}/permissions', 'Acl\RoleController@showRolePermissions')->name('roles.permissions');
	Route::post('/acl/roles/{roleId}/permissions', 'Acl\RoleController@addingPermissionToRole')->name('roles.permissions.add');
	Route::delete('/acl/roles/{roleId}/permissions', 'Acl\RoleController@removingPermissionFromRole')->name('roles.permissions.remove');

	Route::get('/acl/users/{userId}/roles', 'Acl\UserController@editingRolesUser')->name('users.roles');
	Route::post('/acl/users/{userId}/roles', 'Acl\UserController@addingRoleToUser')->name('users.roles.add');
	Route::delete('/acl/users/{userId}/roles', 'Acl\UserController@removingRoleFromUser')->name('users.roles.remove');

	Route::get('/acl/permissions', 'Acl\PermissionController@index')->name('permissions.index');
	Route::get('/acl/permissions/{id}', 'Acl\PermissionController@show')->name('permissions.show');

	Route::get('/profile', 'ContactController@index')->name('profiles.index');
	Route::patch('/profile', 'ContactController@update')->name('profiles.update');

	// Blog Post
	Route::get('/articles/new', 'PostController@newPost' )->name('blogs.create');
	Route::post('/articles', 'PostController@store' )->name('blogs.store');
	Route::patch('/articles/{id}', 'PostController@update')->name('blogs.update');
	Route::get('/articles/{slug}/edit', ['as' => 'blogs.edit', 'uses' => 'PostController@edit'])->where('slug', '[A-Za-z0-9-_]+');

	// Comments
	Route::post('/comments', 'CommentController@store' )->name('comments.store');
	Route::patch('/comments/{id}/disable', 'CommentController@disableComments' )->name('comments.disable');

	// Bookmarks
	Route::get('/bookmarks', 'BookmarkController@index' )->name('bookmarks');
	Route::get('/bookmarks/new', 'BookmarkController@create' )->name('bookmarks.create');
	Route::post('/bookmarks', 'BookmarkController@store' )->name('bookmarks.store');
	Route::get('/bookmarks/{id}', 'BookmarkController@edit')->name('bookmarks.edit');
	Route::patch('/bookmarks/{id}', 'BookmarkController@update')->name('bookmarks.update');
	Route::delete('/bookmarks/{id}', 'BookmarkController@destroy')->name('bookmarks.delete');
	Route::get('/bookmarks/{id}/download', 'BookmarkController@exportToJSON')->name('bookmarks.download');
	Route::post('/bookmarks/{id}/upload', 'BookmarkController@importJSON')->name('bookmarks.upload');
});

// Blogs public routes
Route::get('/articles/dashboard', 'PostController@index' )->name('blogs');
Route::get('/articles', 'PostController@articleList' )->name('blogs.list');

Route::get('/articles/{slug}', ['as' => 'blogs.show', 'uses' => 'PostController@show'])->where('slug', '[A-Za-z0-9-_]+');








